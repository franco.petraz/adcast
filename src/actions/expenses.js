import uuid from 'uuid';
import database from '../firebase/firebase';

// ADD_PODCAST
export const addExpense = (expense) => ({
  type: 'ADD_PODCAST',
  expense
});

export const startAddExpense = (expenseData = {}) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;
    const {
      description = '',
      nameOwner = '',
      emailOwner = '',
      note = '',
      subs = 0,
      createdAt = 0,
      frequencyValue = [],
      ageValue = [],
      geoValue = [],
      genderValue = [],
      industryVal = []
    } = expenseData;
    const expense = { description, nameOwner, emailOwner, note, subs, createdAt, frequencyValue, ageValue, geoValue, genderValue, industryVal};

    return database.ref(`podcasts`).push(expense).then((ref) => {
      dispatch(addExpense({
        id: ref.key,
        ...expense
      }));
    });
  };
};

// REMOVE_PODCAST
export const removeExpense = ({ id } = {}) => ({
  type: 'REMOVE_PODCAST',
  id
});

export const startRemoveExpense = ({ id } = {}) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;
    return database.ref(`podcasts/${id}`).remove().then(() => {
      dispatch(removeExpense({ id }));
    });
  };
};

// EDIT_PODCAST
export const editExpense = (id, updates) => ({
  type: 'EDIT_PODCAST',
  id,
  updates
});

export const startEditExpense = (id, updates) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;
    return database.ref(`podcasts/${id}`).update(updates).then(() => {
      dispatch(editExpense(id, updates));
    });
  };
};

// SET_EXPENSES
export const setExpenses = (expenses) => ({
  type: 'SET_PODCASTS',
  expenses
});

export const startSetExpenses = () => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;
    return database.ref(`podcasts`).once('value').then((snapshot) => {
      const expenses = [];

      snapshot.forEach((childSnapshot) => {
        expenses.push({
          id: childSnapshot.key,
          ...childSnapshot.val()
        });
      });

      dispatch(setExpenses(expenses));
    });
  };
};
