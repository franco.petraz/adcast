import { firebase, googleAuthProvider } from '../firebase/firebase';


export const checkout = () => ({
  type: 'CHECKOUT',
  uid
});

export const StartCheckout = () => {
  return () => {
    const STRIPE_PUBLIC_KEY = 'pk_test_Chve1FS7b7tiBd370cl72AAV'; // TODO: PUT YOUR STRIPE PUBLISHABLE KEY HERE
    const FIREBASE_FUNCTION = 'https://us-central1-adcast-c9da1.cloudfunctions.net/charge/'; // TODO: PUT YOUR FIREBASE FUNCTIONS URL HERE
    const stripe = Stripe(STRIPE_PUBLIC_KEY);
    const elements = stripe.elements();
    const charge_amount = 300;
    const charge_currency = 'usd';
    // Store the elements used
    const elError = document.getElementById('error');
    const elProcessing = document.getElementById('processing');
    const elThanks = document.getElementById('thanks');
    let currentUser = null;
    let stripeCustomerInitialized = false;
    let emailO = "-@-.-";



    pago();

      function pago() {
      firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
          currentUser = firebaseUser.uid;
          addCheckoutMethod();
        } else {
          return firebase.auth().signInWithPopup(googleAuthProvider);
          const currentUser = null;
        }
        });
      };


      // Checks email- Is called byaddCheckoutMethod
        function checkEmail(currentUser) {
            firebase.database()
                   .ref(`/users/${currentUser}/userEmail`)
                   .once('value', snapshot => {
                   emailO = snapshot.val()
                   console.log(emailO);
                   return emailO
                 });
          }


    function addCheckoutMethod() {
        const handler = StripeCheckout.configure({
            key: STRIPE_PUBLIC_KEY,
            locale: 'auto',
            token: async token => {
                // Pass the received token to our Firebase function
                let res = await charge(token, charge_amount, charge_currency);
                //if (res.body.error) return elError.textContent = res.body.error;

                // Card successfully chargedEl
                // elThanks.style.display = 'block';
            }
        });

            handler.open({
                email: emailO || checkEmail(currentUser), // Es asynch por eso no se muestra. Hay que encontrarle la vuelta
                name: 'Subscription',
                amount: charge_amount,
                currency: charge_currency,

        });
        // Close Checkout on page navigation
        window.addEventListener('popstate', () => handler.close());
    }

    // Function used by all three methods to send the charge data to your Firebase function
    async function charge(token, amount, currency) {

      firebase.database().ref(`/users/${currentUser}/pro-membership`).update({
        token: token.id
      });
    }
    }
  };
