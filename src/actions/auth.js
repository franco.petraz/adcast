import { firebase, googleAuthProvider } from '../firebase/firebase';

export const login = (uid) => ({
  type: 'LOGIN',
  uid
});

export const startLogin = () => {
  return () => {
    return firebase.auth().signInWithPopup(googleAuthProvider);  //, EmailAuthProvider(email, password)
  };
};

export const signUp = (newUser) => {
  return (dispatch, getState) => {
//    const firestore = getFirestore();
    firebase.auth().createUserWithEmailAndPassword(
      newUser.emailValue,
      newUser.passValue )
      // .then((resp) => {
      //   return firestore.collection('users').doc(resp.user.uid)
      // })
      .then(() => {
        dispatch({ type: 'SIGNUP_SUCCESS'})
      }).catch(err => {
        dispatch({ type: 'SIGNUP_ERROR'})
      })
    }
};

export const signIn = (credentials) => {
  return (dispatch, getState) => {
//    const firestore = getFirestore();
    firebase.auth().signInWithEmailAndPassword(
      credentials.emailValue,
      credentials.passValue )
      // .then((resp) => {
      //   return firestore.collection('users').doc(resp.user.uid)
      // })
      .then(() => {
        dispatch({ type: 'LOGIN_SUCCESS'})
      }).catch((err) => {
        dispatch({ type: 'LOGIN_ERROR', err})
      })
    }
};


export const logout = () => ({
  type: 'LOGOUT'
});

export const startLogout = () => {
  return () => {
    return firebase.auth().signOut();
  };
};
