import React from 'react';
import ExpenseList from './ExpenseList';
import ExpensesSummary from './ExpensesSummary';
import Footer from './Footer'

const ExpenseDashboardPage = () => (
  <div>
    <ExpensesSummary />
    <ExpenseList />
    <Footer />
  </div>
);

export default ExpenseDashboardPage;
