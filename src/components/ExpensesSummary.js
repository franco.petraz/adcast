import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ExpenseListFilters from './ExpenseListFilters';
import numeral from 'numeral';
import selectExpenses from '../selectors/expenses';
import selectExpensesTotal from '../selectors/expenses-total';

export const ExpensesSummary = ({ expenseCount, expensesTotal }) => {
  const expenseWord = expenseCount === 1 ? 'podcaster' : 'podcasters';

  return (
    <div className="page-header1">
      <div className="content-container">
        <div className="header__content">
        <div><h1 className="page-header__title">Viewing <span>{expenseCount}</span> {expenseWord}</h1></div>
        <div className="page-header__actions">
        </div>
        <div><ExpenseListFilters /></div>
        </div>
      </div>
    </div>
  );
};



const mapStateToProps = (state) => {
  const visibleExpenses = selectExpenses(state.expenses, state.filters);

  return {
    expenseCount: visibleExpenses.length,
    expensesTotal: selectExpensesTotal(visibleExpenses)
  };
};

export default connect(mapStateToProps)(ExpensesSummary);
