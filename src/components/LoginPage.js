import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { startLogin } from '../actions/auth';
import ModalComp from './ModalComp';
import SignIn from './SignIn';
import ReactModal from 'react-modal';
import ExpenseList from './ExpenseList';
import Footer from './Footer'


const LoginPage = ({startLogin}) => (
  <div>
  <div className="bg-hero">
  <div className="content-container">
      <div className="header__content">
      <Link className="header__title header__content" to="/dashboard">
        <img width="40px" height="40px" src={"./images/Logo1.png"} /><h1>AdCast</h1>
      </Link>
      <div className="header__content">
        <ModalComp />
        <SignIn />
      </div>
    </div>

    <div>
    <div className="hero">
        <div className="header__content hero__padding">
            <div className="pd--wrapper" >
            <h1 className="hero__title">Connecting podcasters with advertisers</h1>
            <h2 className="hero__subtitle">Get to promote in a podcast people LISTEN to!</h2>
            </div>

            <div className="pd pd--right">
            <img src="images/jeqyrsr8.png?h=500&amp;fix=crop" className="hero__img" />
            </div>
        </div>
    </div>
    </div>
</div>

<div className="divider">
<svg viewBox="0 0 1695 57" preserveAspectRatio="none"><path d="M0 23c135.4 19 289.6 28.5 462.5 28.5C721.9 51.5 936.7 1 1212.2 1 1395.8.9 1556.7 8.3 1695 23v34H0V23z" fill="#ffffff" fillRule="evenodd"></path></svg>
</div>

  </div>
  <br />
 <ExpenseList />

  <Footer />
  </div>

);

const mapDispatchToProps = (dispatch) => ({
  startLogin: () => dispatch(startLogin()),
});

//TODO: Eliminate this and test
export default connect(undefined, mapDispatchToProps)(LoginPage);
