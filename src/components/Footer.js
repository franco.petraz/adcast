import React from 'react';



const Footer = () => (

  <div>
  <div className="divider">
    <svg viewBox="0 0 1695 72" preserveAspectRatio="none"><path d="M0 0c282.5 45 565 67.5 847.5 67.5S1412.5 45 1695 0v72H0V0z" fill="#fafafa" fillRule="evenodd"></path></svg>
  </div>

    <div className="last">
     <div className="content-container">
          <h2 className="section__title center">Are you a podcaster?</h2>
          <h3 className="section__subtitle center">If you want to be contacted by sponsors click the button below</h3>

           <div className="textBody mkd textBody--center textBody--dark">
               <div className="wr heroWr center">
                  <a className="button__header btn-sg" href="https://linhub.typeform.com/to/EKVg7l" data-mode="popup" data-hide-headers="true" data-hide-footer="true" data-submit-close-delay="5" target="_blank">Count me in! ;) </a>

                  <div>
                  <br />
                  <br />
                  </div>
              </div>
          </div>
    </div>
    </div>
    </div>

);

export default Footer;
