import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
// import Select from './selectForm';

export default class ExpenseForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      frequencyValue: props.expense ? props.expense.frequencyValue : [],
      genderValue: props.expense ? props.expense.genderValue : [],
      industryVal: props.expense ? props.expense.industryVal : [],
      geoValue: props.expense ? props.expense.geoValue : [],
      ageValue: props.expense ? props.expense.ageValue : [],
      description: props.expense ? props.expense.description : '',
      nameOwner: props.expense ? props.expense.nameOwner : '',
      emailOwner: props.expense ? props.expense.emailOwner : '',
      note: props.expense ? props.expense.note : '',
      subs: props.expense ? (props.expense.subs / 100).toString() : '',
      createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
      calendarFocused: false,
      error: ''
    };

  }

  industryChange = (event) => {
      //const ageGroups = event.target.value;
      const industryVal = [...event.target.options].filter(o => o.selected).map(o => o.value);
      console.log(industryVal);
      this.setState(() => ({ industryVal }));
    };

  frecuencyChange = (event) => {
      //const ageGroups = event.target.value;
      const frequencyValue = [...event.target.options].filter(o => o.selected).map(o => o.value);
      console.log(frequencyValue);
      this.setState(() => ({ frequencyValue }));
    };

  onGenderGroupChange = (event) => {
      //const ageGroups = event.target.value;
      const genderValue = [...event.target.options].filter(o => o.selected).map(o => o.value);
      console.log(genderValue);
      this.setState(() => ({ genderValue }));
    };

onGeoGroupChange = (event) => {
    //const ageGroups = event.target.value;
    const geoValue = [...event.target.options].filter(o => o.selected).map(o => o.value);
    console.log(geoValue);
    this.setState(() => ({ geoValue }));
  };

onAgeGroupChange = (event) => {
  //const ageGroups = event.target.value;
  const ageValue = [...event.target.options].filter(o => o.selected).map(o => o.value);
  console.log(ageValue);
  this.setState(() => ({ ageValue }));
};

  onDescriptionChange = (e) => {
    const description = e.target.value;
    this.setState(() => ({ description }));
  };
  nameOwnerChange = (e) => {
    const nameOwner = e.target.value;
    this.setState(() => ({ nameOwner }));
  };
  emailOwnerChange = (e) => {
    const emailOwner = e.target.value;
    this.setState(() => ({ emailOwner }));
  };
  onNoteChange = (e) => {
    const note = e.target.value;
    this.setState(() => ({ note }));
  };
  onAmountChange = (e) => {
    const subs = e.target.value;

    if (!subs || subs.match(/^\d{1,}(\.\d{0,2})?$/)) {
      this.setState(() => ({ subs }));
    }
  };
  onDateChange = (createdAt) => {
    if (createdAt) {
      this.setState(() => ({ createdAt }));
    }
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calendarFocused: focused }));
  };
  onSubmit = (e) => {
    e.preventDefault();

    if (!this.state.description || !this.state.subs) {
      this.setState(() => ({ error: 'Please provide all the info below.' }));
    } else {
      this.setState(() => ({ error: '' }));
      this.props.onSubmit({
        description: this.state.description,
        nameOwner: this.state.nameOwner,
        emailOwner: this.state.emailOwner,
        subs: parseFloat(this.state.subs, 10) * 100,
        createdAt: this.state.createdAt.valueOf(),
        note: this.state.note,
        frequencyValue : this.state.frequencyValue,
        industryVal : this.state.industryVal,
        genderValue : this.state.genderValue,
        geoValue : this.state.geoValue,
        ageValue : this.state.ageValue,
      });
    }
  };
  render() {
    return (
      <form className="form" onSubmit={this.onSubmit}>
        {this.state.error && <p className="form__error">{this.state.error}</p>}
        <input
          type="text"
          placeholder="Podcast Name"
          autoFocus
          className="text-input"
          value={this.state.description}
          onChange={this.onDescriptionChange}
        />
        <input
          type="text"
          placeholder="Add a tagline for your podcast"
          className="text-input"
          value={this.state.note}
          onChange={this.onNoteChange}
        />
        <input
          type="text"
          placeholder="# Subscribers"
          className="text-input"
          value={this.state.subs}
          onChange={this.onAmountChange}
        />
        <SingleDatePicker
          date={this.state.createdAt}
          onDateChange={this.onDateChange}
          focused={this.state.calendarFocused}
          onFocusChange={this.onFocusChange}
          numberOfMonths={1}
          isOutsideRange={() => false}
        />
        <input
          type="text"
          placeholder="Owner's Name"
          className="text-input"
          value={this.state.nameOwner}
          onChange={this.nameOwnerChange}
        />
        <input
          type="text"
          placeholder="podcaster@best.com"
          className="text-input"
          value={this.state.emailOwner}
          onChange={this.emailOwnerChange}
        />
        <select onChange={this.frecuencyChange} value={this.state.frequencyValue} multiple={true}>
          <option value="Daily">Daily</option>
          <option value="Weekly">Weekly</option>
          <option value="Twice a month">Twice a month</option>
          <option value="Monthly">Monthly</option>
          <option value="Not defined">Not defined</option>
        </select>
        <select onChange={this.onAgeGroupChange} value={this.state.ageValue} multiple={true}>
          <option value="Under 18">Under 18</option>
          <option value="18-24">18-24</option>
          <option value="25-30">25-30</option>
          <option value="31-40">31-40</option>
          <option value="41+">41+</option>
        </select>
        <select onChange={this.onGeoGroupChange} value={this.state.geoValue} multiple={true}>
          <option value="Worldwide">Worldwide</option>
          <option value="North America">North America</option>
          <option value="South America">South America</option>
          <option value="Europe">Europe</option>
          <option value="Asia">Asia</option>
          <option value="Africa">Africa</option>
        </select>
        <select onChange={this.onGenderGroupChange} value={this.state.genderValue} multiple={true}>
          <option value="Everyone">Everyone</option>
          <option value="Males">Males</option>
          <option value="Females">Females</option>
          <option value="Gender-fluid / LGTB">Gender-fluid / LGTB</option>
        </select>
        <select onChange={this.industryChange} value={this.state.industryVal} multiple={true}>
          <option value="Food">Food</option>
          <option value="Business owners">Business owners</option>
          <option value="Entrepreneurs">Entrepreneurs</option>
          <option value="Cryptocurrency">Cryptocurrency</option>
          <option value="Parenting">Parenting</option>
          <option value="Finance">Finance</option>
          <option value="Programmers">Programmers</option>
          <option value="Creatives">Creatives</option>
          <option value="Tech">Tech</option>
          <option value="Entertainment">Entertainment</option>
          <option value="Comedy">Comedy</option>
          <option value="Sports">Sports</option>
          <option value="News & Politics">News & Politics</option>
          <option value="Gaming">Gaming</option>
          <option value="Religion & Spirituality">Religion & Spirituality</option>
          <option value="Education">Education</option>
          <option value="Learning">Learning</option>
          <option value="Geeks">Geeks</option>
        </select>

        <div>
          <button className="button button--right">Add Podcast</button>
        </div>
      </form>
    )
  }
}
