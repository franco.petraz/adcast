  import React, { Component } from 'react';
  import { connect } from 'react-redux';
  import ReactDOM from 'react-dom';
  import Modal from 'react-modal';
  import { startLogin, signUp } from '../actions/auth';

//http://reactcommunity.org/react-modal/

  export class ModalComp extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        modalIsOpen: false,
        emailValue: '',
        passValue: '',
        error: ''
       };
    }

    openModal = () => {
      this.setState({modalIsOpen: true});
    }

    closeModal = () => {
      this.setState({modalIsOpen: false});
    }

    handleModalCloseRequest = () => {
      // opportunity to validate something and keep the modal open even if it
      // requested to be closed
      this.setState({modalIsOpen: false});
    }

    handleSaveClicked = (e) => {
      alert('Save button was clicked');
    }

    startLoginHandler = (e) => {
    this.props.startLogin();
  }

  handleChange = (e) => {
    this.setState({
          [e.target.id]: e.target.value})
  };


  handleSubmit = (e) => {
    e.preventDefault();
    if (!this.state.emailValue || !this.state.passValue) {
      this.setState(() => ({ error: 'Please provide an email and a password.' }));
    } else {
      this.setState(() => ({ error: '' }));
      this.props.signUp({
        emailValue: this.state.emailValue,
        passValue: this.state.passValue
      });
    }
  };

    render() {
      const { authError } = this.props;
      return (
        <div>
          <button type="button" className="button__header btn-sg" onClick={this.openModal}>Sign Up</button>
          <Modal
            className="Modal__Bootstrap modal-dialog"
            closeTimeoutMS={150}
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.handleModalCloseRequest}
          >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Sign Up</h4>
                <button type="button" className="close" onClick={this.handleModalCloseRequest}>
                  <span aria-hidden="true">&times;</span>
                  <span className="sr-only">Close</span>
                </button>
              </div>
              <div className="modal-body">
                <button type="button" className="button__log" onClick={ this.startLoginHandler }>
                  <img width="16" height="16" src={"./images/google.svg"} />
                   Sign Up with Google Account
                </button>
              </div>
                <div className="middle_separator modal-body"> or </div>

              <form className="form modal-body" onSubmit={this.handleSubmit}>
                <input type="email" placeholder="Email" name="email" className="input input_email" id="emailValue" tabIndex="1" value={this.state.emailValue} onChange={this.handleChange} />
                <input type="password" placeholder="Password" name="password" className="input input_password" tabIndex="2" id="passValue" value={this.state.passValue} onChange={this.handleChange} />
                <button type="submit" className="button__log-red">
                  Sign Up with Email
                </button>
              </form>
              <div className="errorLog">
              { authError ? <p>{authError}</p> : null }
              </div>
            </div>
          </Modal>
        </div>

      );
    }
  }

  const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError
  }
}

  const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin()),
    signUp: (newUser) => dispatch(signUp(newUser))
  });

  export default connect(mapStateToProps, mapDispatchToProps)(ModalComp);
