import React from 'react';
import { connect } from 'react-redux';
import ExpenseListItem from './ExpenseListItem';
import ExpenseListItemSub from './ExpenseListItemSub';
import selectExpenses from '../selectors/expenses';
import { firebase } from '../firebase/firebase';

let currentUser = null;
firebase.auth().onAuthStateChanged(firebaseUser => {
  if (firebaseUser) {
    currentUser = firebaseUser.uid;
  }
  });

export class ExpenseList extends React.Component {

      constructor(props) {
        super(props);
        this.state = {
          memberShip: 'Not Logged' ,     //props ? props.membership :
          status: ''
        };
      }

      componentDidMount() {
       var promise = new Promise( (resolve, reject) => {
          let memberShip = "unknown"

          if (currentUser != null) {
            firebase.database()
                 .ref(`/users/${currentUser}/pro-membership`)
                 .once('value')
                 .then(snapshot => {
                   console.log(snapshot.val().status)
//                   if (snapshot.val() != null) {
                      memberShip = snapshot.val().status;
                              if (memberShip === 'active') {
                                   resolve(memberShip);
                                  }
                                  else {
                                   reject(Error("PastDue"));
                                  }
                   // }
                   // else {
                   //   memberShip = "NeverSubscribed";
                   // }
             });
          } else {
            resolve('Not subscribed')
          }

       });

//TODO: Check why the LogIn page isn't working properly.
       promise.then( result => {
        this.setState({memberShip: result});
       }, function(error) {
        this.setState({memberShip: error});
       });
      }

  render() {
    return (
      <div className="content-container">
        <div className="list-header">
          <div className="show-for-mobile">Podcasts</div>
          <div className="show-for-desktop">Podcasts</div>
          <div className="show-for-desktop"></div>
        </div>
        <div className="list-body">

          {
            this.state.memberShip === "active" ? (
              this.props.expenses.map((expense) => {
                  return <ExpenseListItemSub key={expense.id} {...expense} />;
                })
              ) : (
            this.props.expenses.map((expense) => {
                  return <ExpenseListItem key={expense.id} {...expense} />;
                })
              )
          }

        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    expenses: selectExpenses(state.expenses, state.filters)
    //membership: membership(state.membership) // modif Header
    }
  };

export default connect(mapStateToProps)(ExpenseList);
