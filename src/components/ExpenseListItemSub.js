import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import numeral from 'numeral';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ExpenseListItemSub = ({ id, description, subs, createdAt, note, ageValue, geoValue, genderValue, industryVal, frequencyValue, nameOwner, emailOwner }) => (
  <div className="list-item">
        <div className="description-box">
      <h3 className="list-item__title  list-item__name">{description}</h3>
      <p className="list-item__title">{note}</p>
      <br />
      <span className="list-item__sub-title">{moment(createdAt).format('MMMM Do, YYYY')}</span>
      <p className="list-item__title"><FontAwesomeIcon icon="calendar-alt" /> {frequencyValue}</p>
    </div>

    <div className="description-box list-item__title">
            <h4 className="list-item__sub-title">Age Group:
            <span>
            { ageValue ?
              ageValue.map((age) => {
                return <a key={age} className="tag" title={age}> {age} </a>;    //href=`/filters/{age}`
              })
              : "-" }
            </span></h4>

            <h4 className="list-item__sub-title">Location:
            <span>
            { geoValue ?
              geoValue.map((geo) => {
                return <a key={geo} className="tag" title={geo}> {geo} </a>;    //href=`/filters/{age}`
              })
              : "-" }
            </span></h4>

            <h4 className="list-item__sub-title">Gender:
            <span>
            { genderValue ?
              genderValue.map((gender) => {
                return <a key={gender} className="tag" title={gender}> {gender} </a>;    //href=`/filters/{age}`
              })
              : "-" }
            </span></h4>

            <h4 className="list-item__sub-title">Industry:
            <span>
            { industryVal ?
              industryVal.map((ind) => {
                return <a key={ind} className="tag" title={ind}> {ind} </a>;    //href=`/filters/{age}`
              })
              : "-" }
            </span></h4>
    </div>

    <div className="description-box">
    <h3 className="list-item__data list-item__name" title="Number of users"><FontAwesomeIcon icon="user" /> {numeral(subs / 100).format('0,0')} subscribers</h3>
    <br />
    <h4 className="list-item__title" title="Podcaster's name"><FontAwesomeIcon icon="address-card" /> {nameOwner}</h4>
    <h4 className="list-item__title" title="e-mail"><FontAwesomeIcon icon="envelope" /> {emailOwner}</h4>
    </div>
  </div>
);

export default ExpenseListItemSub;
