import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { startLogout } from '../actions/auth';
import CheckOutButton from './CheckOutButton';

export const Header = ({ startLogout }) => (
  <header className="header">
    <div className="content-container">
      <div className="header__content">
        <Link className="header__title header__content" to="/dashboard">
          <img width="40px" height="40px" src={"./images/Logo1.png"} /> <h1>AdCast</h1>
        </Link>
        <div className="header__content">
          <CheckOutButton />
          <button className="button__header btn-si" onClick={startLogout}>Logout</button>
        </div>

      </div>
    </div>
  </header>
);

//En CheckOutButton Meter condicional si ya esta registrado (reutilizar Selector e membershipS usado en EpenseList)

const mapDispatchToProps = (dispatch) => ({
  startLogout: () => dispatch(startLogout())
});

export default connect(undefined, mapDispatchToProps)(Header);
