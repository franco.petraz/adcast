import React from 'react';
import { connect } from 'react-redux';
import { StartCheckout } from '../actions/checkout';

export const Checkout = ({ StartCheckout }) => (

  <div>
      <div>
            <button className="button__header btn-sg" id="checkout" onClick={StartCheckout}>Subscribe</button>
            <div id="error"></div>
        {/* <div id="processing" className="checkout-error">processing...</div>
        <div id="thanks" className="checkout-error">Thanks for your donation!</div> */}
    </div>
  </div>
);

const mapDispatchToProps = (dispatch) => ({
  StartCheckout: () => dispatch(StartCheckout())
});

export default connect(undefined, mapDispatchToProps)(Checkout);






// import React from 'react';
// import { connect } from 'react-redux';
// import { StartCheckout } from '../actions/checkout';
// import { firebase } from '../firebase/firebase';
//
//
// let currentUser = null;
// firebase.auth().onAuthStateChanged(firebaseUser => {
//   if (firebaseUser) {
//     currentUser = firebaseUser.uid;
//   }
//   });
// let emailO = '';
//
// export class Checkout extends React.Component {
//
//   constructor(props) {
//     super(props);
//     this.state = {
//       memberShip: '' ,
//       status: '',
//       emailO: ''
//     };
//   };
//
//   componentDidMount() {
//    var promise = new Promise( (resolve, reject) => {
//       let memberShip = "unknown"
//
//       firebase.database()
//            .ref(`/users/${currentUser}/pro-membership`)
//            .once('value')
//            .then(snapshot => {
//            memberShip = snapshot.val().status || "NeverSubscribed";
//             if (memberShip === 'active') {
//               resolve(memberShip);
//               // Necesito meter un async para que aparezca el email
//               // firebase.database()
//               //        .ref(`/users/${currentUser}/userEmail`)
//               //        .once('value', snapshot => {
//               //          emailO = snapshot.val();
//               //          console.log(emailO);
//               //        });
//             }
//             else {
//              reject(Error("PastDue"));
//             }
//        });
//
//    });
//
//
//
//    promise.then( result => {
//     this.setState({memberShip: result});
//    }, function(error) {
//     this.setState({memberShip: error});
//    });
//   }
//
//       render() {
//         return (
//
//       <div>
//           <div>
//           {
//             this.state.memberShip === "active" ?
//                   <h4  className="button">{ emailO || "Subscribed!" }</h4>
//                :
//                   <button className="button" id="checkout" onClick={StartCheckout}>Subscribe</button>
//                 }
//
//             {/* <div id="processing" className="checkout-error">processing...</div>
//             <div id="thanks" className="checkout-error">Thanks for your donation!</div> */}
//         </div>
//       </div>
//     );
//     }
// }
//
// const mapDispatchToProps = (dispatch) => ({
//   StartCheckout: () => dispatch(StartCheckout())
// });
//
// export default connect(undefined, mapDispatchToProps)(Checkout);
