// PODCASTS Reducer

const expensesReducerDefaultState = [];

export default (state = expensesReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_PODCAST':
      return [
        ...state,
        action.expense
      ];
    case 'REMOVE_PODCAST':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_PODCAST':
      return state.map((expense) => {
        if (expense.id === action.id) {
          return {
            ...expense,
            ...action.updates
          };
        } else {
          return expense;
        };
      });
    case 'SET_PODCASTS':
      return action.expenses;
    default:
      return state;
  }
};
