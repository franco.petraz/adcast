export default (state = {}, action) => {
  switch (action.type) {
    case 'CHECKOUT':
    return {
      uid: action.uid
    };
    default:
      return state;
  }
};
