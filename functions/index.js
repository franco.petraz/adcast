const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const express = require('express');
const cors = require('cors')({origin: true});
const app = express();

// TODO: Remember to set token using >> firebase functions:config:set stripe.token="SECRET_STRIPE_TOKEN_HERE"
const stripe = require('stripe')(functions.config().stripe.token);

// When a user is created, register them with Stripe
exports.createStripeCustomer = functions.auth.user().onCreate((user) => {

  return stripe.customers.create({
    email: user.email,
  })
	.then(customer => {
  /// update database with stripe customer id

  const data = { customerId: customer.id }

      const updates = {}
      updates[`/customers/${customer.id}`]     = user.uid
      updates[`/users/${user.uid}/customerId`] = customer.id
      updates[`/users/${user.uid}/userEmail`] = user.email

      return admin.database().ref().update(updates);
	});
});


// When a user deletes their account, clean up after them
exports.cleanupUser = functions.auth.user().onDelete((user) => {
return admin.database().ref(`/stripe_customers/${user.uid}`).once('value').then(
(snapshot) => {
return snapshot.val();
}).then((customer) => {
return stripe.customers.del(customer.customer_id);
}).then(() => {
return admin.database().ref(`/stripe_customers/${user.uid}`).remove();
});
});


exports.updateToken = functions.database.ref('/users/{uid}/pro-membership/token').onWrite((change, context) => {

  const tokenId = change.after.val(); // data after the write
  const uid  = context.params.uid;

  if (!tokenId) throw new Error('token missing');

      return admin.database()
      .ref(`/users/${uid}/customerId`)
      .once('value')
      .then(snapshot => snapshot.val())
      .then(customerId => {
        return stripe.customers.createSource(customerId, {
          source: tokenId,
        });
        })
      .then((source) => {
        return stripe.subscriptions.create({
              customer: source.customer,
              items: [
                  {
                    plan: 'plan_Dpbw4M8iOiNvEO',
                  },
                ],
              });
      })
      .then((subscribe) => {
        // New charge created on a new customer
      })
      .catch(err => console.log(err))
    });



exports.recurringPayment = functions.https.onRequest((req, res) => {

    const hook  = req.body.type
    const data  = req.body.data.object

    if (!data) throw new Error('missing data')

    return admin.database()
      .ref(`/customers/${data.customer}`)
    	.once('value')
    	.then(snapshot => snapshot.val())
    	.then((currentUser) => {
      	const ref = admin.database().ref(`/users/${currentUser}/pro-membership`)

          // Handle successful payment webhook
          if (hook === 'invoice.payment_succeeded') {
      			return ref.update({ status: 'active' });
   				 }

          // Handle failed payment webhook
          if (hook === 'invoice.payment_failed') {
            return ref.update({ status: 'pastDue' });
          }

       })
       .then(() => res.status(200).send(`successfully handled ${hook}`) )
       .catch(err => res.status(400).send(`error handling ${hook}`))

});
